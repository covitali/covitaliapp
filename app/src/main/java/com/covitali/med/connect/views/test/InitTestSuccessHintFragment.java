/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import com.covitali.med.connect.R;

/**
 * Display init test successfully finished message to user.
 *
 * Created by Chen Xue on 2020/02/20.
 * @version 1.0
 */

public class InitTestSuccessHintFragment extends TestInitBaseFragment {

    @Override
    protected void setViews() {
        testInitTitleTxt.setText(R.string.str_process_8_title);
        testInitMsgTxt.setText(R.string.str_process_8_message);
    }

    @Override
    protected void onCountDownFinished() {
        // after 5 sec then start successfully finish init test view
        InitTestStateController.getInstance().showNextView();
    }

    @Override
    public void onStart() {
        super.onStart();

        // set up 5 seconds countdown timer
        startTimer(5);
    }
}
