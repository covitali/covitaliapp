/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.adapater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.model.MedicamentPlan;

import java.util.ArrayList;

/**
 * Medicament plan list adapter
 *
 * Created by chen on 25.02.20
 * @version 1.0
 */
public class MedicamentPlanListAdapter extends RecyclerView.Adapter<MedicamentPlanListAdapter.ViewHolder>  {
    private ArrayList<MedicamentPlan> items = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.medicament_plan_list_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.medicamentPlanListMsgTxt.setText(items.get(position).getMessage());
        holder.medicamentPlanListDateTxt.setText(items.get(position).getDateTime());
        holder.medicamentPlanListDoctorTxt.setText(items.get(position).getDoctor());
    }

    public void updateList(ArrayList<MedicamentPlan> plans) {
        if (plans == null) plans = new ArrayList<>();
        items = plans;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView medicamentPlanListMsgTxt;
        private TextView medicamentPlanListDateTxt;
        private TextView medicamentPlanListDoctorTxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            medicamentPlanListMsgTxt = (TextView) itemView.findViewById(R.id.medicament_plan_list_msg_txt);
            medicamentPlanListDateTxt = (TextView) itemView.findViewById(R.id.medicament_plan_list_date_txt);
            medicamentPlanListDoctorTxt = (TextView) itemView.findViewById(R.id.medicament_plan_list_doctor_txt);
        }
    }
}
