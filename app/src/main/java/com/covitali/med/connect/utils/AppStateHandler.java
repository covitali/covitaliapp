/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.utils;

import android.content.Context;

import com.covitali.med.connect.AppApplication;
import com.covitali.med.connect.model.InitTestState;

import at.apptec.atlibrary.utils.ATUtilsSharedPref;

/**
 * Base class for activities with common methods
 * <p>
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */
public class AppStateHandler {
    private static AppStateHandler instance = null;
    private AppApplication appApplication = null;

    private static final String KEY_INIT_TEST_STATE = "KEY_INIT_TEST_STATE";

    /**
     * Singleton: private constructor
     */
    private AppStateHandler() {

    }

    /**
     * Singleton: get AppStateHandler instance
     * @return AppStateHandler
     */
    public static AppStateHandler getInstance() {
        if (null == instance) {
            synchronized (AppStateHandler.class) {
                if (null == instance) {
                    instance = new AppStateHandler();
                }
            }
        }
        return instance;
    }

    /**
     * If app started, muss init this AppStateHandler first for the app basic operation
     * @param appApplication return {@link com.covitali.med.connect.AppApplication} instance
     */
    public void init(AppApplication appApplication) {
        this.appApplication = appApplication;
    }

    /**
     * Get global application context
     * @return {@code Context} return global {@link android.content.Context} instance
     */
    public Context getApplicationContext() {
        return appApplication.getApplicationContext();
    }

    /**
     * Check if app is still alive
     * @return boolean
     */
    private boolean isAppAlive() {
        return appApplication != null;
    }

    /**
     *  check init test state
     * @return {@code boolean} if current state is
     * {@link com.covitali.med.connect.model.InitTestState#STATE_TEST_FINISH_SUCCESS} then return
     * {@code true} otherwise return {@code false}.
     */
    public boolean isInitTestStateFinished() {
        int id = ATUtilsSharedPref.getInt(getApplicationContext(), KEY_INIT_TEST_STATE, 0);
        return (InitTestState.fromId(id) == InitTestState.STATE_TEST_FINISH_SUCCESS);
    }

    /**
     * Save init test state.
     *
     * @param state {@code InitTestState} the current {@link com.covitali.med.connect.model.InitTestState}
     */
    public void saveInitTestState(InitTestState state) {
        ATUtilsSharedPref.putInt(getApplicationContext(), KEY_INIT_TEST_STATE, state.getId());
    }
}
