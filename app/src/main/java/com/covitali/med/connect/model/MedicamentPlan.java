/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.model;

import android.text.TextUtils;

import org.json.JSONObject;

/**
 * Created by chen on 25.02.20
 * @version 1.0
 */
public class MedicamentPlan {
    private String id = "";
    private String message = "";
    private String type = "";
    private String date = "";
    private String time = "";
    private String doctor = "";
    private String sms = "";
    private String serverMessage = "";
    private String eCallNum = "";
    private boolean shouldConfirm = false;
    private String confirmationIntervalStr = "24";
    private String confirmationPeriodStr = "5";
    private long confirmationIntervalInMills = 24L * 60L * 60L * 1000L;
    private long confirmationPeriodInMills = 5L * 60L * 1000L;

    public MedicamentPlan(JSONObject jsonObject) {
        if (jsonObject == null) return;
        id = jsonObject.optString("id", "");
        type = jsonObject.optString("type", "");
        message = jsonObject.optString("text", "");
        date = jsonObject.optString("date", "");
        time = jsonObject.optString("time", "");
        sms = jsonObject.optString("sms", "");
        serverMessage = jsonObject.optString("serverMessage", "");
        eCallNum = jsonObject.optString("eCallNumber", "");
        shouldConfirm = "1".equals(jsonObject.optString("confirm", ""));

        String intervalStr = jsonObject.optString("confirmationInterval", "");
        if (!TextUtils.isEmpty(intervalStr) && !"null".equals(intervalStr) && intervalStr.contains(".")) {
            String[] strs = intervalStr.split(".");
            if (strs.length >= 2) {
                confirmationIntervalStr = strs[0];
                confirmationPeriodStr = strs[1];
                try {
                    confirmationIntervalInMills = Long.parseLong(confirmationIntervalStr) * 60L * 60L * 1000L;
                    confirmationPeriodInMills = Long.parseLong(confirmationPeriodStr) * 60L * 1000L;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        // meta data
        doctor = jsonObject.optString("doctor", "");
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getDateTime() {
        return date + " " + time;
    }

    public long getConfirmationIntervalInMills() {
        return confirmationIntervalInMills;
    }

    public long getConfirmationPeriodInMills() {
        return confirmationPeriodInMills;
    }

    public String getConfirmationIntervalStr() {
        return confirmationIntervalStr;
    }

    public String getConfirmationPeriodStr() {
        return confirmationPeriodStr;
    }

    public String geteCallNum() {
        return eCallNum;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public String getSms() {
        return sms;
    }

    public String getDoctor() {
        return doctor;
    }
}
