/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.repositories;

import com.covitali.med.connect.AppApplication;
import com.covitali.med.connect.events.EventMedicamentPlansLoaded;
import com.covitali.med.connect.model.MedicamentPlan;
import com.covitali.med.connect.utils.Properties;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * Data repositories
 *
 * Created by chen on 25.02.20
 * @version 1.0
 */
public class Repositories {
    private static Repositories instance = null;
    private AppApplication appApplication = null;

    private ArrayList<MedicamentPlan> medicamentPlans = new ArrayList<>();

    /**
     * Singleton: private constructor
     */
    private Repositories() {

    }

    /**
     * Singleton: get Repositories instance
     * @return Repositories
     */
    public static Repositories getInstance() {
        if (null == instance) {
            synchronized (Repositories.class) {
                if (null == instance) {
                    instance = new Repositories();
                }
            }
        }
        return instance;
    }

    /**
     * If app started, muss init this Repositories first for the app basic operation
     * @param appApplication return {@link com.covitali.med.connect.AppApplication} instance
     */
    public void init(AppApplication appApplication) {
        this.appApplication = appApplication;
    }

    private void loadMedicamentPlansFromServer() {
        // TODO: load date from server
        String jsonStr = "[{\"id\":0,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Herzwohl\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 08:00\"},{\"id\":1,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Mexalen\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 08:00\"},{\"id\":2,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Herzwohl\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 12:00\"},{\"id\":3,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Mexalen\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 12:00\"},{\"id\":4,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Herzwohl\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 18:00\"},{\"id\":5,\"message\":\"Zeit zur Einnahme Ihres Medikaments -> Mexalen\",\"doctor\":\"Dr. Harnold\",\"date_time\":\"28.02.2020 18:00\"}]";
        try {
            JSONArray jsonArray = new JSONArray(jsonStr);
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                medicamentPlans.add(new MedicamentPlan(jsonArray.optJSONObject(i)));
            }
            ATUtilsEventBus.post(new EventMedicamentPlansLoaded(Properties.RESPONSE_SUCCESS, medicamentPlans));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadMedicamentPlans() {
        if (medicamentPlans == null || medicamentPlans.size() == 0) {
            loadMedicamentPlansFromServer();
        } else {
            ATUtilsEventBus.post(new EventMedicamentPlansLoaded(Properties.RESPONSE_SUCCESS, medicamentPlans));
        }
    }
}
