/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.reminders;

import android.view.View;
import android.widget.TextView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.utils.Utils;
import com.covitali.med.connect.views.base.BaseActivity;

/**
 * Stored reminders are presented to the user at the given date and time and have to be
 * acknowledged if it is marked to be acknowledged.
 *
 * Created by Chen Xue on 2020/02/24.
 * @version 1.0
 */
public class RemindersActivity extends BaseActivity {
    private TextView reminderTxt;
    private View reminderFlashingFrame;


    @Override
    protected int getContentView() {
        return R.layout.reminder_activity;
    }

    @Override
    protected void assignViews() {
        reminderTxt = findViewById(R.id.reminder_txt);
        reminderFlashingFrame = findViewById(R.id.reminder_flashing_frame);
    }

    @Override
    protected void prepareViews() {
        // TODO: set reminder text
        reminderTxt.setText(R.string.str_process_5_reminder_confirm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.startFlashingFrameAnim(reminderFlashingFrame);
    }
}
