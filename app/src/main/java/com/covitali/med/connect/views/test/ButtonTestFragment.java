/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.view.KeyEvent;

import com.covitali.med.connect.R;
import com.covitali.med.connect.events.EventKeyUp;
import com.covitali.med.connect.model.InitTestState;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import at.apptec.atlibrary.utils.ATUtils;
import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * Button press test, user must in 10 seconds press the specific button.
 *
 * Created by Chen Xue on 2020/02/20.
 * @version 1.0
 */

public class ButtonTestFragment extends TestInitBaseFragment {

    @Override
    protected void setViews() {
        testInitTitleTxt.setText(R.string.str_process_1_title);
        testInitMsgTxt.setText(R.string.str_process_1_message);
    }

    @Override
    protected void onCountDownFinished() {
        // press button in 10 sec failed, start test button failed view
        InitTestStateController.getInstance().showView(InitTestState.STATE_BUTTON_TEST_FAILED);
    }

    @Override
    public void onStart() {
        super.onStart();
        ATUtilsEventBus.register(this);

        // set up 10 seconds countdown timer, wait user to press button
        startTimer(10);
    }

    @Override
    public void onStop() {
        super.onStop();
        ATUtilsEventBus.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventKeyUp event) {
        if (ATUtils.isDoubleClick()) return;
        if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
            // stop timer and start next process
            stopTimer();
            InitTestStateController.getInstance().showNextView();
        }
    }
}
