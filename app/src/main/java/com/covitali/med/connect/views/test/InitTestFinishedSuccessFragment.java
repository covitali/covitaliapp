/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.content.Intent;

import com.covitali.med.connect.R;
import com.covitali.med.connect.views.plan.MedicamentPlanActivity;

/**
 * Display the init test finished successfully and after 5 seconds start {@link com.covitali.med.connect.views.plan.MedicamentPlanActivity}
 *
 * Created by Chen Xue on 2020/02/20.
 * @version 1.0
 */

public class InitTestFinishedSuccessFragment extends TestInitBaseFragment {

    @Override
    protected void setViews() {
        testInitTitleTxt.setText(R.string.str_init_test_finished_success_title);
        testInitMsgTxt.setText(R.string.str_init_test_finished_success_message);
    }

    @Override
    protected void onCountDownFinished() {
        // after 10 seconds then start MainActivity
        startMainActivity();
    }

    @Override
    public void onStart() {
        super.onStart();

        // start 5 seconds countdown timer
        startTimer(10);
    }

    /**
     * Start {@link com.covitali.med.connect.views.plan.MedicamentPlanActivity}
     */
    private void startMainActivity() {
        if (getActivity() != null) {
            Intent intent = new Intent(getApplicationContext(), MedicamentPlanActivity.class);
            startActivity(intent);
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

}
