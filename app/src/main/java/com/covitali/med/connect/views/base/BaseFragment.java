/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Base class for fragments with common methods
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */
public abstract class BaseFragment extends Fragment {
    protected View fragmentView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(getFragmentContentView(), container, false);
        assignViews();
        return fragmentView;
    }

    /**
     * init fragment with layout resource id
     * @return {@code int} return layout resource id
     */
    @LayoutRes
    protected abstract int getFragmentContentView();

    /**
     * assign views on create view
     */
    protected abstract void assignViews();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareViews();
    }

    /**
     * prepare views after fragment view created
     */
    protected abstract void prepareViews();

    /**
     * find view by resource id
     * @return View
     */
    protected  <T extends View> T findViewById(@IdRes int id) {
        return fragmentView.findViewById(id);
    }

    /**
     * get application context
     * @return Context
     */
    protected Context getApplicationContext() {
        if (getActivity() != null) {
            return getActivity().getApplicationContext();
        }

        if (getContext() != null) {
            return getContext().getApplicationContext();
        }

        return fragmentView.getContext().getApplicationContext();
    }
}
