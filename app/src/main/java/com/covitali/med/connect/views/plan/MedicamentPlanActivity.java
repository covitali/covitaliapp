/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.plan;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.adapater.MedicamentPlanListAdapter;
import com.covitali.med.connect.events.EventMedicamentPlansLoaded;
import com.covitali.med.connect.model.MedicamentPlan;
import com.covitali.med.connect.repositories.Repositories;
import com.covitali.med.connect.utils.Properties;
import com.covitali.med.connect.views.base.BaseActivity;
import com.covitali.med.connect.views.metadata.MetaDataActivity;
import com.covitali.med.connect.views.test.InitTestActivity;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import at.apptec.atlibrary.utils.ATUtils;
import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * A separate screen is shown where the complete list of reminders is displayed.
 *
 * Created by chen on 25.02.20
 * @version 1.0
 */
public class MedicamentPlanActivity extends BaseActivity {
    private RecyclerView medicamentPlanList;
    private TextView medicamentPlanLogoTxt;
    private Button medicamentPlanInitTestBtn;
    private Button medicamentPlanMetaDataBtn;

    private MedicamentPlanListAdapter adapter = null;


    @Override
    protected int getContentView() {
        return R.layout.medicament_plan_activity;
    }

    @Override
    protected void assignViews() {
        medicamentPlanList = (RecyclerView) findViewById(R.id.medicament_plan_list);
        medicamentPlanLogoTxt = (TextView) findViewById(R.id.medicament_plan_logo_txt);
        medicamentPlanInitTestBtn = (Button) findViewById(R.id.medicament_plan_init_test_btn);
        medicamentPlanMetaDataBtn = (Button) findViewById(R.id.medicament_plan_meta_data_btn);
    }

    @Override
    protected void prepareViews() {
        medicamentPlanList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
        medicamentPlanList.addItemDecoration(dividerItemDecoration);

        medicamentPlanInitTestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ATUtils.isDoubleClick()) return;
                // start init test activity
                startInitTestActivity();
            }
        });

        medicamentPlanMetaDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ATUtils.isDoubleClick()) return;
                // start meta data activity
                startMetaDataActivity();
            }
        });
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = new MedicamentPlanListAdapter();
        }

        if (medicamentPlanList.getAdapter() == null) {
            medicamentPlanList.setAdapter(adapter);
        }
    }

    private void updateList(ArrayList<MedicamentPlan> plans) {
        initAdapter();
        adapter.updateList(plans);
    }

    /**
     * Start {@link com.covitali.med.connect.views.test.InitTestActivity}
     */
    private void startInitTestActivity() {
        Intent intent = new Intent(getApplicationContext(), InitTestActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Start {@link com.covitali.med.connect.views.metadata.MetaDataActivity}
     */
    private void startMetaDataActivity() {
        Intent intent = new Intent(getApplicationContext(), MetaDataActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ATUtilsEventBus.register(this);
        // TODO: show loading dialog
        Repositories.getInstance().loadMedicamentPlans();
    }

    @Override
    protected void onStop() {
        ATUtilsEventBus.unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMedicamentPlansLoaded event) {
        if (Properties.RESPONSE_SUCCESS.equals(event.getCode())) {
            updateList(event.getItems());
        } else {
            // TODO: show error message
        }
    }
}
