/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.metadata;

import com.covitali.med.connect.R;
import com.covitali.med.connect.views.base.BaseActivity;

/**
 * A separate screen is shown where meta information about the originator of the reminder list is displayed.
 *
 * Created by chen on 25.02.20
 * @version 1.0
 */
public class MetaDataActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.meta_data_activity;
    }

    @Override
    protected void assignViews() {

    }

    @Override
    protected void prepareViews() {

    }
}
