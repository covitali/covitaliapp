/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.splash;

import android.content.Intent;
import android.os.CountDownTimer;

import com.covitali.med.connect.R;
import com.covitali.med.connect.utils.AppStateHandler;
import com.covitali.med.connect.views.base.BaseActivity;
import com.covitali.med.connect.views.plan.MedicamentPlanActivity;
import com.covitali.med.connect.views.test.InitTestActivity;

/**
 * Display app splash view and show next view after 3 seconds
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.splash_activity;
    }

    /**
     * assign views on create activity
     */
    @Override
    protected void assignViews() {

    }

    /**
     * prepare views on create activity
     */
    @Override
    protected void prepareViews() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        // start timer
        startTimer();
    }

    /**
     * Start timer with 3 seconds to show splash view when app starts.
     * When countdown timer is finished, check init test state first, if the current init test state
     * is {@link com.covitali.med.connect.model.InitTestState#STATE_TEST_FINISH_SUCCESS} then start
     * {@link com.covitali.med.connect.views.plan.MedicamentPlanActivity}, otherwise start
     * {@link com.covitali.med.connect.views.test.InitTestActivity}
     *
     */
    private void startTimer() {
        CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (AppStateHandler.getInstance().isInitTestStateFinished()) {
                    startMainActivity();
                } else {
                    startInitTestActivity();
                }
            }
        };
        countDownTimer.start();
    }

    /**
     * Start {@link com.covitali.med.connect.views.plan.MedicamentPlanActivity}
     */
    private void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MedicamentPlanActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Start {@link com.covitali.med.connect.views.test.InitTestActivity}
     */
    private void startInitTestActivity() {
        Intent intent = new Intent(getApplicationContext(), InitTestActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
