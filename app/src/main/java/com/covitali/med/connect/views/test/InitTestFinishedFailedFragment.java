/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.view.View;
import android.widget.Button;

import com.covitali.med.connect.R;
import com.covitali.med.connect.events.EventKeyUp;
import com.covitali.med.connect.model.InitTestState;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import at.apptec.atlibrary.utils.ATUtils;
import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * Created by Chen Xue on 2020/02/20.
 *
 * @version 1.0
 */

public class InitTestFinishedFailedFragment extends TestInitBaseFragment {
    private Button yesBtn;
    private Button noBtn;

    @Override
    protected int getFragmentContentView() {
        return R.layout.test_init_button_fragment;
    }

    @Override
    protected void setViews() {
        yesBtn = findViewById(R.id.test_init_yes_btn);
        noBtn = findViewById(R.id.test_init_no_btn);

        testInitTitleTxt.setText(R.string.str_init_test_finished_failed_title);
        testInitMsgTxt.setText(R.string.str_init_test_finished_failed_message);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitTestStateController.getInstance().showView(InitTestState.STATE_BUTTON_TEST);
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitTestStateController.getInstance().showView(InitTestState.STATE_TEST_FINISH_FAILED);
            }
        });
    }

    @Override
    protected void onCountDownFinished() {
        // after 1 minute no user response then close the app
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ATUtilsEventBus.register(this);

        // set up 1 minute countdown timer, wait user response, if no user response then close the app.
        startTimer(60);
    }

    @Override
    public void onStop() {
        super.onStop();
        ATUtilsEventBus.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventKeyUp event) {
        if (ATUtils.isDoubleClick()) return;
        startTimer(60);
    }
}
