/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect;


import com.covitali.med.connect.views.base.BaseActivity;

/**
 * Init test state controller, control current state
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */
public class MainActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.main_activity;
    }

    /**
     * assign views on create activity
     */
    @Override
    protected void assignViews() {

    }

    /**
     * prepare views on create activity
     */
    @Override
    protected void prepareViews() {

    }
}
