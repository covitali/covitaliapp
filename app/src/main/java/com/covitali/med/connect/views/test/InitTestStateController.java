/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.covitali.med.connect.R;
import com.covitali.med.connect.model.InitTestState;
import com.covitali.med.connect.utils.AppStateHandler;

/**
 * Init test state controller, control current state
 * <p>
 * Created by Chen Xue on 2020/02/19.
 *
 * @version 1.0
 */
public class InitTestStateController {
    private static InitTestStateController instance = null;

    private FragmentManager fragmentManager;
    private int fragmentContainer = 0;

    private Fragment currentFragment = null;
    private InitTestState currentTestState = null;

    private int buttonTestFailedCount = 0;
    private int confirmTestFailedCount = 0;

    /**
     * Singleton: private constructor
     */
    private InitTestStateController() {

    }

    /**
     * Singleton: get InitTestStateController instance
     * @return InitTestStateController
     */
    public static InitTestStateController getInstance() {
        if (null == instance) {
            synchronized (InitTestStateController.class) {
                if (null == instance) {
                    instance = new InitTestStateController();
                }
            }
        }
        return instance;
    }


    /**
     * Instantiates the InitTestStateController.
     *
     * @param fragmentManager   the fragment manager
     * @param fragmentContainer the fragment container resource id
     */
    public void init(FragmentManager fragmentManager, @IdRes int fragmentContainer) {
        this.fragmentManager = fragmentManager;
        this.fragmentContainer = fragmentContainer;
    }



    public void showView(InitTestState testState) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (currentFragment != null) {
            fragmentTransaction.remove(currentFragment);
        }

        // if try to start button test view and already have tried it 5 times then change the state
        // to STATE_TEST_FINISH_FAILED
        if (testState == InitTestState.STATE_BUTTON_TEST && buttonTestFailedCount >= 6) {
            testState = InitTestState.STATE_TEST_FINISH_FAILED;
        }

        // if try to start confirm reminder test view and already have tried it 5 times then change the state
        // to STATE_TEST_FINISH_FAILED
        if (testState == InitTestState.STATE_CONFIRM_TEST_REMINDER && confirmTestFailedCount >= 6) {
            testState = InitTestState.STATE_TEST_FINISH_FAILED;
        }

        switch (testState) {
            case STATE_BUTTON_TEST:
                currentFragment = new ButtonTestFragment();
                break;
            case STATE_BUTTON_TEST_SUCCESS:
                currentFragment = new ButtonTestSuccessFragment();
                break;
            case STATE_BUTTON_TEST_FAILED:
                buttonTestFailedCount++;
                currentFragment = new ButtonTestFailedFragment();
                break;
            case STATE_DISPLAY_HINT:
                currentFragment = new ReminderTestHintFragment();
                break;
            case STATE_START_TEST_REMINDER:
                currentFragment = new ReminderStartTestFragment();
                break;
            case STATE_CONFIRM_TEST_REMINDER:
                currentFragment = new ReminderConfirmFragment();
                break;
            case STATE_TEST_REMINDER_SUCCESS:
                currentFragment = new ReminderConfirmSuccessFragment();
                break;
            case STATE_TEST_REMINDER_FAILED:
                confirmTestFailedCount++;
                currentFragment = new ReminderConfirmFailedFragment();
                break;
            case STATE_DISPLAY_TEST_SUCCESS_HINT:
                currentFragment = new InitTestSuccessHintFragment();
                break;
            case STATE_TEST_FINISH_SUCCESS:
                currentFragment = new InitTestFinishedSuccessFragment();
                AppStateHandler.getInstance().saveInitTestState(InitTestState.STATE_TEST_FINISH_SUCCESS);
                break;
            case STATE_TEST_FINISH_FAILED:
                currentFragment = new InitTestFinishedFailedFragment();
                break;
            case STATE_WELCOME:
            default:
                currentFragment = new WelcomeFragment();
                break;
        }

        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(fragmentContainer, currentFragment);
        fragmentTransaction.commit();

        currentTestState = testState;
    }

    public void showNextView() {
        InitTestState nextState = null;
        switch (currentTestState) {
            case STATE_WELCOME:
                nextState = InitTestState.STATE_BUTTON_TEST;
                break;
            case STATE_BUTTON_TEST:
                nextState = InitTestState.STATE_BUTTON_TEST_SUCCESS;
                break;
            case STATE_BUTTON_TEST_SUCCESS:
                nextState = InitTestState.STATE_DISPLAY_HINT;
                break;
            case STATE_BUTTON_TEST_FAILED:
                nextState = InitTestState.STATE_TEST_FINISH_FAILED;
                break;
            case STATE_DISPLAY_HINT:
                nextState = InitTestState.STATE_START_TEST_REMINDER;
                break;
            case STATE_START_TEST_REMINDER:
                nextState = InitTestState.STATE_CONFIRM_TEST_REMINDER;
                break;
            case STATE_CONFIRM_TEST_REMINDER:
                nextState = InitTestState.STATE_TEST_REMINDER_SUCCESS;
                break;
            case STATE_TEST_REMINDER_SUCCESS:
                nextState = InitTestState.STATE_DISPLAY_TEST_SUCCESS_HINT;
                break;
            case STATE_TEST_REMINDER_FAILED:
                nextState = InitTestState.STATE_TEST_FINISH_FAILED;
                break;
            case STATE_DISPLAY_TEST_SUCCESS_HINT:
                nextState = InitTestState.STATE_TEST_FINISH_SUCCESS;
                break;
            default:
                nextState = InitTestState.STATE_WELCOME;
                break;
        }

        showView(nextState);
    }

    public void showFirstView() {
        showView(InitTestState.STATE_WELCOME);
    }

    /**
     * get current init test state
     *
     * @return InitTestState get current InitTestState instance
     */
    public InitTestState getCurrentTestState() {
        return currentTestState;
    }

    public void onDestroy() {
        currentTestState = null;
        currentFragment = null;
        buttonTestFailedCount = 0;
        confirmTestFailedCount = 0;
    }
}
