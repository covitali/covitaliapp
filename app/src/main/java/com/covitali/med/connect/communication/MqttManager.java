/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.communication;

import org.json.JSONObject;


/**
 * Created by Chen Xue on 2020/02/17.
 * @version 1.0
 */
public class MqttManager implements Communicator {
    // https://blog.csdn.net/xiao_sier/article/details/78651414
    /**
     * get server url
     * @return {@code String} return server url in {@code String}
     */
    @Override
    public String getServerUrl() {
        return null;
    }

    /**
     * init communicator
     */
    @Override
    public void init() {

    }

    /**
     * reconnect server
     */
    @Override
    public void reconnect() {

    }

    /**
     * send message to server
     * @param jsonMsg send message in {@code JSONObject} to server
     * @return {@code boolean} the message has successfully sent to server then return {@code true},
     * otherwise return {@code false}.
     */
    @Override
    public boolean sendMessage(JSONObject jsonMsg) {
        return false;
    }

    /**
     * save log message in local
     * @param log save log message in {@code String} in local
     * @return {@code boolean} the log message has successfully saved then return {@code true},
     * otherwise return {@code false}.
     */
    @Override
    public boolean logReport(String log) {
        return false;
    }

    /**
     * destroy communicator
     */
    @Override
    public void destroy() {

    }
}
