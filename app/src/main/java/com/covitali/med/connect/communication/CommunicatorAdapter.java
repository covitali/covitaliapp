/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.communication;

import org.json.JSONObject;

/**
 * Created by Chen Xue on 2020/02/17.
 * @version 1.0
 */

public class CommunicatorAdapter implements Communicator {
    public static final String SEVER_PROTOCOL_MQTT = "MQTT";
    public static final String SEVER_PROTOCOL_TCP = "TCP";
    public static final String DEFAULT_SEVER_PROTOCOL = SEVER_PROTOCOL_MQTT;


    private static CommunicatorAdapter instance = null;

    private Communicator communicator = null;

    /**
     * singleton: private constructor
     */
    private CommunicatorAdapter() {
    }

    /**
     * singleton: get communicator instance
     * @return {@code CommunicatorAdapter}
     */
    public static CommunicatorAdapter getInstance() {
        if (null == instance) {
            synchronized (CommunicatorAdapter.class) {
                if (null == instance) {
                    instance = new CommunicatorAdapter();
                }
            }
        }
        return instance;
    }

    /**
     * get server url
     * @return {@code String} return server url in {@code String}
     */
    @Override
    public String getServerUrl() {
        if (this.communicator != null) {
            return communicator.getServerUrl();
        }
        return "";
    }

    /**
     * init communicator with specific server protocol
     */
    @Override
    public void init() {
        String protocol = DEFAULT_SEVER_PROTOCOL;
        // TODO: get current server protocol from setting
        communicator = initCommunicator(protocol);
        communicator.init();
    }

    /**
     * init communicator with current protocol
     * @param serverProtocol current server connect protocol {@link CommunicatorAdapter#SEVER_PROTOCOL_MQTT} in {@code String}
     * @return {@code Communicator}
     */
    private Communicator initCommunicator(String serverProtocol) {
        if (serverProtocol == null) {
            serverProtocol = SEVER_PROTOCOL_MQTT;
        }
        if (SEVER_PROTOCOL_MQTT.equals(serverProtocol)) {
            return new MqttManager();
        }
        if (SEVER_PROTOCOL_TCP.equals(serverProtocol)) {
            return new TcpManager();
        }
        return new MqttManager();
    }

    /**
     * reconnect server
     */
    @Override
    public void reconnect() {
        if (communicator != null) {
            communicator.reconnect();
        }
    }

    /**
     * send message to server
     * @param jsonMsg send message in {@code JSONObject} to server
     * @return {@code boolean} the message has successfully sent to server then return {@code true},
     * otherwise return {@code false}.
     */
    @Override
    public boolean sendMessage(JSONObject jsonMsg) {
        boolean done = false;
        if (communicator != null) {
            done = communicator.sendMessage(jsonMsg);
        }
        return done;
    }

    /**
     * save log message in local
     * @param log save log message in {@code String} in local
     * @return {@code boolean} the log message has successfully saved then return {@code true},
     * otherwise return {@code false}.
     */
    @Override
    public boolean logReport(String log) {
        boolean done = false;
        if (communicator != null) {
            done = communicator.logReport(log);
        }
        return done;
    }

    /**
     * destroy communicator
     */
    @Override
    public void destroy() {
        if (communicator != null) {
            communicator.destroy();
        }
    }
}
