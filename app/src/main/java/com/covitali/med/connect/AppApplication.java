/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.widget.Toast;

import androidx.multidex.MultiDexApplication;

import com.covitali.med.connect.repositories.Repositories;
import com.covitali.med.connect.utils.AppStateHandler;
import com.covitali.med.connect.views.plan.MedicamentPlanActivity;

import at.apptec.atlibrary.utils.ATUtilsLog;

/**
 * The type App application.
 *
 * Created by Chen Xue on 2020/02/17.
 * @version 1.0
 */
public class AppApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        initGlobalParameters();

        initAppParameters();
    }

    private void initGlobalParameters() {
        if (getApplicationContext().getResources().getBoolean(R.bool.debug)) {
            ATUtilsLog.openAll();
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .penaltyLog()
                    .detectAll()
                    .permitDiskReads()
                    .permitDiskWrites()
                    .penaltyDeathOnNetwork()
                    .penaltyDialog()
                    .build());
        } else {
            ATUtilsLog.closeAll();

            // Setup handler for uncaught exceptions.
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable e) {
                    handleUncaughtException(thread, e);
                }
            });
        }
    }

    /**
     * Handle uncaught exception.
     *
     * @param thread    the thread
     * @param exception the exception
     */
    public void handleUncaughtException(Thread thread, Throwable exception) {
        exception.printStackTrace(); // not all Android versions will print the stack trace automatically

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Oops, We apologize for the issues! The app restart now!", Toast.LENGTH_LONG).show();
            }
        });

        // After catching an exception, restart
        Intent intent = new Intent(this, MedicamentPlanActivity.class);
        // required when starting from Application
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        android.os.Process.killProcess(android.os.Process.myPid());
        // kill off the crashed app
        System.exit(1);
    }

    private void initAppParameters() {
        AppStateHandler.getInstance().init(this);
        Repositories.getInstance().init(this);
    }
}
