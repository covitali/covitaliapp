/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.model;

public enum InitTestState {
    /**
     * Display welcome message to user.
     */
    STATE_WELCOME(0),

    /**
     * Press watch button test.
     */
    STATE_BUTTON_TEST(1),

    /**
     * Press watch button success.
     */
    STATE_BUTTON_TEST_SUCCESS(2),

    /**
     * Press watch button failed.
     */
    STATE_BUTTON_TEST_FAILED(3),

    /**
     * Display the hints to user how to use.
     */
    STATE_DISPLAY_HINT(4),

    /**
     * Display the test reminder to use.
     */
    STATE_START_TEST_REMINDER(5),

    /**
     * Tests whether the user correctly performed the specified action.
     */
    STATE_CONFIRM_TEST_REMINDER(6),

    /**
     * The user performed the specified action correctly.
     */
    STATE_TEST_REMINDER_SUCCESS(7),

    /**
     * The user performed the specified action incorrectly.
     */
    STATE_TEST_REMINDER_FAILED(8),

    /**
     * Display the test success message to user.
     */
    STATE_DISPLAY_TEST_SUCCESS_HINT(9),

    /**
     * The test finished successfully.
     */
    STATE_TEST_FINISH_SUCCESS(10),

    /**
     * The test failed.
     */
    STATE_TEST_FINISH_FAILED(11);

    private int id;

    private InitTestState(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static InitTestState fromId(int id) {
        for (InitTestState type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }
}
