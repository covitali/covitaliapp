/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import com.covitali.med.connect.R;

/**
 * Display the welcome init test view to user, and set 8 seconds countdown timer, if time is up
 * then start next process.
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */

public class WelcomeFragment extends TestInitBaseFragment {

    @Override
    protected void setViews() {
        testInitTitleTxt.setText(R.string.str_welcome);
        testInitMsgTxt.setText(R.string.str_welcome_msg);
    }

    @Override
    public void onStart() {
        super.onStart();

        // set up 8 seconds countdown timer to start next process
        startTimer(8);
    }

    @Override
    protected void onCountDownFinished() {
        // start next process
        InitTestStateController.getInstance().showNextView();
    }
}
