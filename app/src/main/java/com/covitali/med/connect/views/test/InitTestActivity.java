/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.covitali.med.connect.R;
import com.covitali.med.connect.events.EventKeyUp;
import com.covitali.med.connect.views.base.BaseActivity;

import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * Before a patient can start using the app (and periodically recurring) he has to pass a quick
 * initial test to check if he is able to understand and handle the system.
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */
public class InitTestActivity extends BaseActivity {
    private FrameLayout fragmentContainer = null;

    @Override
    protected int getContentView() {
        return R.layout.test_init_activity;
    }

    /**
     * assign views on create activity
     */
    @Override
    protected void assignViews() {
        fragmentContainer = findViewById(R.id.test_init_fragment_container);
    }

    /**
     * prepare views on create activity
     */
    @Override
    protected void prepareViews() {
        InitTestStateController.getInstance().init(getSupportFragmentManager(), R.id.test_init_fragment_container);
        fragmentContainer.post(new Runnable() {
            @Override
            public void run() {
                InitTestStateController.getInstance().showFirstView();
            }
        });
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        ATUtilsEventBus.post(new EventKeyUp(keyCode));
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        InitTestStateController.getInstance().onDestroy();
        super.onDestroy();
    }
}
