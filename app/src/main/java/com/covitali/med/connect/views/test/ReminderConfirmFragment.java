/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.events.EventKeyUp;
import com.covitali.med.connect.model.InitTestState;
import com.covitali.med.connect.utils.Utils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import at.apptec.atlibrary.utils.ATUtils;
import at.apptec.atlibrary.utils.ATUtilsEventBus;

/**
 * Display test message and reminder to user, user must in 10 seconds press the confirm button on watch.
 * If not then start show reminder confirm test failed view.
 * <p>
 * Created by Chen Xue on 2020/02/20.
 *
 * @version 1.0
 */

public class ReminderConfirmFragment extends TestInitBaseFragment {
    private TextView reminderText;
    private View flashingFrame;

    @Override
    protected int getFragmentContentView() {
        return R.layout.test_init_reminder_fragment;
    }

    @Override
    protected void setViews() {
        reminderText = findViewById(R.id.test_init_reminder_txt);
        flashingFrame = findViewById(R.id.test_init_flashing_frame);

        testInitTitleTxt.setText(R.string.str_process_5_title);
        testInitMsgTxt.setText(R.string.str_process_5_message);
        reminderText.setText(R.string.str_process_5_reminder_confirm);
    }

    @Override
    protected void onCountDownFinished() {
        // after 10 seconds start confirm reminder test view
        InitTestStateController.getInstance().showView(InitTestState.STATE_TEST_REMINDER_FAILED);
    }

    /**
     * start to display the test reminder with sound and vibration
     */
    private void startReminder() {
        // set up 10 seconds countdown timer
        startTimer(10);
        Utils.startFlashingFrameAnim(flashingFrame);

        try {
            // play notification sound
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();

            // play vibration
            if (getActivity() != null) {
                Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    //deprecated in API 26
                    v.vibrate(500);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ATUtilsEventBus.register(this);
        startReminder();
    }

    @Override
    public void onStop() {
        super.onStop();
        ATUtilsEventBus.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventKeyUp event) {
        if (ATUtils.isDoubleClick()) return;
        if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
            // stop timer and start next process
            stopTimer();
            InitTestStateController.getInstance().showNextView();
        }
    }
}
