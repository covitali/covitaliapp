/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.view.View;
import android.widget.TextView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.utils.Utils;

/**
 * Display reminder test message and reminder to user, after 5 seconds start reminder confirm test.
 *
 * Created by Chen Xue on 2020/02/20.
 * @version 1.0
 */

public class ReminderStartTestFragment extends TestInitBaseFragment {
    private TextView reminderText;
    private View flashingFrame;

    @Override
    protected int getFragmentContentView() {
        return R.layout.test_init_reminder_fragment;
    }

    @Override
    protected void setViews() {
        reminderText = findViewById(R.id.test_init_reminder_txt);
        flashingFrame = findViewById(R.id.test_init_flashing_frame);

        testInitTitleTxt.setText(R.string.str_process_4_title);
        testInitMsgTxt.setText(R.string.str_process_4_message);
        reminderText.setText(R.string.str_process_4_reminder);
    }

    @Override
    protected void onCountDownFinished() {
        // after 5 seconds start confirm reminder view
        InitTestStateController.getInstance().showNextView();
    }

    @Override
    public void onStart() {
        super.onStart();

        // set up 5 seconds countdown timer
        startTimer(5);
        Utils.startFlashingFrameAnim(flashingFrame);
    }
}
