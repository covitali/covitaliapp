/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.events;

import com.covitali.med.connect.model.BaseEvent;
import com.covitali.med.connect.model.MedicamentPlan;

import java.util.ArrayList;

/**
 * Created by chen on 25.02.20
 *
 * @version 1.0
 */
public class EventMedicamentPlansLoaded extends BaseEvent {
    private ArrayList<MedicamentPlan> items = new ArrayList<>();

    public EventMedicamentPlansLoaded(String code, ArrayList<MedicamentPlan> items) {
        super(code);
        this.items = items;
    }

    public ArrayList<MedicamentPlan> getItems() {
        return items;
    }
}
