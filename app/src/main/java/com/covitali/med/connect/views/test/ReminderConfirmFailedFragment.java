/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.view.View;
import android.widget.TextView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.model.InitTestState;

/**
 * Display reminder confirm test failed message and after 5 seconds restart the reminder confirm test.
 *
 * Created by Chen Xue on 2020/02/20.
 * @version 1.0
 */

public class ReminderConfirmFailedFragment extends TestInitBaseFragment {
    private TextView reminderText;
    private View flashingFrame;

    @Override
    protected int getFragmentContentView() {
        return R.layout.test_init_reminder_fragment;
    }

    @Override
    protected void setViews() {
        reminderText = findViewById(R.id.test_init_reminder_txt);
        flashingFrame = findViewById(R.id.test_init_flashing_frame);

        testInitTitleTxt.setText(R.string.str_process_6_title);
        testInitMsgTxt.setText(R.string.str_process_6_message);
        reminderText.setText(R.string.str_process_6_reminder_confirm);
    }

    @Override
    protected void onCountDownFinished() {
        // after 5 seconds restart confirm reminder test view
        InitTestStateController.getInstance().showView(InitTestState.STATE_CONFIRM_TEST_REMINDER);
    }

    @Override
    public void onStart() {
        super.onStart();

        // set up 5 seconds countdown timer
        startTimer(5);
    }
}
