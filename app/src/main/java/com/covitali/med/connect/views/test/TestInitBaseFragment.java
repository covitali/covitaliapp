/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package com.covitali.med.connect.views.test;

import android.os.CountDownTimer;
import android.text.method.ScrollingMovementMethod;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.covitali.med.connect.R;
import com.covitali.med.connect.views.base.BaseFragment;


/**
 * Base fragment for init test, init fragment, prepare views and init countdown timer.
 *
 * Created by Chen Xue on 2020/02/19.
 * @version 1.0
 */

public abstract class TestInitBaseFragment extends BaseFragment {
    protected TextView testInitTitleTxt;
    protected TextView testInitLogoTxt;
    protected TextView testIniTimerTxt;
    protected TextView testInitMsgTxt;
    protected ProgressBar testInitProgressBar;

    private CountDownTimer countDownTimer = null;
    private boolean isRunning = false;

    @Override
    protected int getFragmentContentView() {
        return R.layout.test_init_message_fragment;
    }

    /**
     * assign views after view created
     */
    @Override
    protected void assignViews() {
        testInitTitleTxt = findViewById(R.id.test_init_title_txt);
        testInitLogoTxt = findViewById(R.id.test_init_logo_txt);
        testIniTimerTxt = findViewById(R.id.test_init_timer_txt);
        testInitMsgTxt = findViewById(R.id.test_init_msg_txt);
        testInitProgressBar = findViewById(R.id.test_init_progressbar);
    }
    /**
     * prepare views after view created
     */
    @Override
    protected void prepareViews() {
        testInitMsgTxt.setMovementMethod(new ScrollingMovementMethod());
        setViews();
    }

    /**
     * set views after view created
     */
    protected abstract void setViews();


    /**
     * Start timer with specific time in second
     * @param second {@code int} the time in seconds should be countdown.
     */
    protected void startTimer(int second) {
        stopTimer();

        final long totalMills = second * 1000;
        countDownTimer = new CountDownTimer(totalMills, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isRunning = true;
                int progress = (int) (((double) millisUntilFinished / (double) totalMills) * 100.0);
//                ATUtilsLog.error("progress:" + progress);
                if (testInitProgressBar != null) {
                    testInitProgressBar.setProgress(progress);
                }
                if (testIniTimerTxt != null) {
                    testIniTimerTxt.setText(String.valueOf((int) ((millisUntilFinished + 1000) / 1000)));
                }
            }

            @Override
            public void onFinish() {
                isRunning = false;
                testInitProgressBar.setProgress(0);
                onCountDownFinished();
            }
        };
        countDownTimer.start();
    }

    /**
     * Stop timer
     */
    protected void stopTimer() {
        if (countDownTimer != null && isRunning) {
            countDownTimer.cancel();
            isRunning = false;
            countDownTimer = null;
        }
    }

    protected abstract void onCountDownFinished();

    @Override
    public void onStop() {
        stopTimer();
        super.onStop();
    }
}
