/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package at.apptec.atlibrary.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import at.apptec.atlibrary.okhttp.OkHttpUtils;
import at.apptec.atlibrary.okhttp.builder.GetBuilder;
import at.apptec.atlibrary.okhttp.builder.OtherRequestBuilder;
import at.apptec.atlibrary.okhttp.builder.PostFormBuilder;
import at.apptec.atlibrary.okhttp.callback.Callback;
import at.apptec.atlibrary.okhttp.request.HeaderParams;
import at.apptec.atlibrary.okhttp.request.RequestCall;
import at.apptec.atlibrary.okhttp.request.RequestParams;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okio.Buffer;

/**
 * Created by chen on 26/05/15.
 */
public class ATUtilsOkHttp {
    public static final int METHOD_GET = 0;
    public static final int METHOD_POST = 1;
    public static final int METHOD_PUT = 2;
    public static final int METHOD_PATCH = 3;
    public static final int METHOD_DELETE = 4;

    /**
     * Asynchronous HTTP client instance
     */
    protected static OkHttpClient client = null;

    private static String credentials = null;

    public static void initHttpClient() {
        client = new OkHttpClient();
        OkHttpUtils.getInstance(client);
    }

    public static void initHttpClient(OkHttpClient okHttpClient) {
        client = okHttpClient;
        OkHttpUtils.getInstance(client);
    }

    public static OkHttpClient getHttpClient() {
        if (client == null)
            initHttpClient();

        return client;
    }


    public static void setAuthClient(String userName, String password) {
        credentials = userName + ":" + password;
    }

    public static void setCertificates(String certificates) {
        if (TextUtils.isEmpty(certificates)) {
            // user https, but access all certificates
            OkHttpUtils.getInstance().setCertificates();
        } else {
            OkHttpUtils.getInstance().setCertificates(new InputStream[]{
                    new Buffer()
                            .writeUtf8(certificates)
                            .inputStream()});
        }
    }

    /**
     * create connection (no parameters)
     *
     * @param method   METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_PATCH, METHOD_DELETE
     * @param url      request URL
     * @param callback callback Handler:
     *                 - AsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BinaryHttpResponseHandler (int statusCode, Header[] headers, byte[] binaryData)
     *                 - DataAsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BaseJsonHttpResponseHandler (int statusCode, Header[] headers, String rawJsonResponse, JSON_TYPE response)
     *                 - JsonHttpResponseHandler (int statusCode, Header[] headers, JSONObject response) / (int statusCode, Header[] headers, JSONArray response)
     *                 - TextHttpResponseHandler (int statusCode, Header[] headers, String responseString)
     *                 - FileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - RangeFileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - SaxAsyncHttpResponseHandler (int statusCode, Header[] headers, T extends org.xml.sax.helpers.DefaultHandler t)
     * @return RequestHandle  request handle, may null!!!!
     */
    public static RequestCall createConnection(int method, String url, Callback callback) {
        if (client == null)
            initHttpClient();

        return create(url, method, callback);
    }


    public static RequestCall createConnection(int method, String url, HeaderParams headerParams, Callback callback) {
        if (client == null)
            initHttpClient();

        return create(url, method, headerParams, callback);
    }

    /**
     * create auth connection (no parameters)
     *
     * @param method   METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_PATCH, METHOD_DELETE
     * @param url      request URL
     * @param callback callback Handler:
     *                 - AsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BinaryHttpResponseHandler (int statusCode, Header[] headers, byte[] binaryData)
     *                 - DataAsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BaseJsonHttpResponseHandler (int statusCode, Header[] headers, String rawJsonResponse, JSON_TYPE response)
     *                 - JsonHttpResponseHandler (int statusCode, Header[] headers, JSONObject response) / (int statusCode, Header[] headers, JSONArray response)
     *                 - TextHttpResponseHandler (int statusCode, Header[] headers, String responseString)
     *                 - FileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - RangeFileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - SaxAsyncHttpResponseHandler (int statusCode, Header[] headers, T extends org.xml.sax.helpers.DefaultHandler t)
     * @return RequestHandle  request handle, may null!!!!
     */
    public static RequestCall createAuthConnection(int method, String url, String userName, String password, Callback callback) {
        if (client == null) {
            initHttpClient();
        }

        if (userName != null && !userName.equals("") && password != null && !password.equals("")) {
            setAuthClient(userName, password);
        }

        return create(url, method, callback);
    }


    /**
     * create connection (with parameters)
     *
     * @param url      request URL
     * @param params   request parameters
     * @param method   METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_PATCH, METHOD_DELETE
     * @param callback callback Handler:
     *                 - AsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BinaryHandler (int statusCode, Header[] headers, byte[] binaryData)
     *                 - DataAsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BaseJsonHttpResponseHandler (int statusCode, Header[] headers, String rawJsonResponse, JSON_TYPE response)
     *                 - JsonHttpResponseHandler (int statusCode, Header[] headers, JSONObject response) / (int statusCode, Header[] headers, JSONArray response)
     *                 - TextHttpResponseHandler (int statusCode, Header[] headers, String responseString)
     *                 - FileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - RangeFileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - SaxAsyncHttpResponseHandler (int statusCode, Header[] headers, T extends org.xml.sax.helpers.DefaultHandler t)
     * @return RequestHandle  request handle, may null!!!!
     */
    public static RequestCall createConnection(int method, String url, RequestParams params, Callback callback) {
        if (client == null)
            initHttpClient();

        return create(url, params, method, callback);
    }


    public static RequestCall createConnection(int method, String url, HeaderParams headerParams, RequestParams requestParams, Callback callback) {
        if (client == null)
            initHttpClient();

        return create(url, headerParams, requestParams, method, callback);
    }

    /**
     * create auth connection (with parameters)
     *
     * @param method   METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_PATCH, METHOD_DELETE
     * @param url      request URL
     * @param params   request parameters
     * @param callback callback Handler:
     *                 - AsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BinaryHandler (int statusCode, Header[] headers, byte[] binaryData)
     *                 - DataAsyncHttpResponseHandler (int statusCode, Header[] headers, byte[] responseBody)
     *                 - BaseJsonHttpResponseHandler (int statusCode, Header[] headers, String rawJsonResponse, JSON_TYPE response)
     *                 - JsonHttpResponseHandler (int statusCode, Header[] headers, JSONObject response) / (int statusCode, Header[] headers, JSONArray response)
     *                 - TextHttpResponseHandler (int statusCode, Header[] headers, String responseString)
     *                 - FileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - RangeFileAsyncHttpResponseHandler (int statusCode, Header[] headers, File file)
     *                 - SaxAsyncHttpResponseHandler (int statusCode, Header[] headers, T extends org.xml.sax.helpers.DefaultHandler t)
     * @return RequestHandle  request handle, may null!!!!
     */
    public static RequestCall createAuthConnection(int method, String url, RequestParams params, String userName, String password, Callback callback) {
        if (client == null)
            initHttpClient();

        if (userName != null && !userName.equals("") && password != null && !password.equals("")) {
            setAuthClient(userName, password);
        }

        return create(url, params, method, callback);
    }


    private static RequestCall create(String url, int method, Callback callback) {
        RequestCall requestCall = null;
        switch (method) {
            case METHOD_GET:
                GetBuilder getBuilder = OkHttpUtils.get().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    getBuilder.addHeader("Authorization", basic);
                }
                requestCall = getBuilder.build();
                requestCall.execute(callback);
                break;
            case METHOD_POST:
                PostFormBuilder postFormBuilder = OkHttpUtils.post().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    postFormBuilder.addHeader("Authorization", basic);
                }
                requestCall = postFormBuilder.build();
                requestCall.execute(callback);
                break;
//            case METHOD_PUT:
//                OtherRequestBuilder putRequestBuilder = OkHttpUtils.put().url(url);
//                if (!TextUtils.isEmpty(credentials)) {
//                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                    putRequestBuilder.addHeader("Authorization", basic);
//                }
//                requestCall = putRequestBuilder.build();
//                requestCall.execute(callback);
//                break;
//            case METHOD_PATCH:
//                OtherRequestBuilder pathRequestBuilder = OkHttpUtils.patch().url(url);
//                if (!TextUtils.isEmpty(credentials)) {
//                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                    pathRequestBuilder.addHeader("Authorization", basic);
//                }
//                requestCall = pathRequestBuilder.build();
//                requestCall.execute(callback);
//                break;
//            case METHOD_DELETE:
//                OtherRequestBuilder deleteRequestBuilder = OkHttpUtils.delete().url(url);
//                if (!TextUtils.isEmpty(credentials)) {
//                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                    deleteRequestBuilder.addHeader("Authorization", basic);
//                }
//                requestCall = deleteRequestBuilder.build();
//                requestCall.execute(callback);
//                break;
        }

        return requestCall;
    }

    private static RequestCall create(String url, int method, HeaderParams headerParams, Callback callback) {
        RequestCall requestCall = null;
        switch (method) {
            case METHOD_GET:
                GetBuilder getBuilder = OkHttpUtils.get().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        getBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
                requestCall = getBuilder.build();
                requestCall.execute(callback);
                break;
            case METHOD_POST:
                PostFormBuilder postFormBuilder = OkHttpUtils.post().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        postFormBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
                requestCall = postFormBuilder.build();
                requestCall.execute(callback);
                break;
        }

        return requestCall;
    }


    private static RequestCall create(String url, RequestParams params, int method, Callback callback) {
        RequestCall requestCall = null;
        if (params == null) params = new RequestParams();
        switch (method) {
            case METHOD_GET:
                GetBuilder getBuilder = OkHttpUtils.get().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    getBuilder.addHeader("Authorization", basic);
                }
                requestCall = getBuilder.addParams(params.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_POST:
                PostFormBuilder postFormBuilder = OkHttpUtils.post();
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    postFormBuilder.addHeader("Authorization", basic);
                }

                if (params.getFileParams().size() > 0) {
                    Map<String, RequestParams.FileWrapper> fileParams = params.getFileParams();
                    for (ConcurrentHashMap.Entry<String, RequestParams.FileWrapper> entry : fileParams.entrySet()) {
                        postFormBuilder.addFile(entry.getKey(), entry.getValue().customFileName, entry.getValue().file);
                    }
                }

                postFormBuilder.url(url);

                Map<String, String> _params = params.getUrlParams();
                if (_params.size() > 0) {
                    postFormBuilder.addParams(_params);
                }
                requestCall = postFormBuilder.build();
                requestCall.execute(callback);
                break;
            case METHOD_PUT:
                OtherRequestBuilder putRequestBuilder = OkHttpUtils.put().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    putRequestBuilder.addHeader("Authorization", basic);
                }
//                requestCall = putRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = putRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(params.getUrlParams()))).addParams(params.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_PATCH:
                OtherRequestBuilder pathRequestBuilder = OkHttpUtils.patch().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    pathRequestBuilder.addHeader("Authorization", basic);
                }
//                requestCall = pathRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = pathRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(params.getUrlParams()))).addParams(params.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_DELETE:
                OtherRequestBuilder deleteRequestBuilder = OkHttpUtils.delete().url(url);
                if (!TextUtils.isEmpty(credentials)) {
                    final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    deleteRequestBuilder.addHeader("Authorization", basic);
                }
//                requestCall = deleteRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = deleteRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(params.getUrlParams()))).addParams(params.getUrlParams()).build();
                requestCall.execute(callback);
                break;
        }

        return requestCall;
    }

    private static RequestCall create(String url, HeaderParams headerParams, RequestParams requestParams, int method, Callback callback) {
        RequestCall requestCall = null;
        if (requestParams == null) requestParams = new RequestParams();
        switch (method) {
            case METHOD_GET:
                GetBuilder getBuilder = OkHttpUtils.get().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        getBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
                requestCall = getBuilder.addParams(requestParams.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_POST:
                PostFormBuilder postFormBuilder = OkHttpUtils.post();
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        postFormBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }

                if (requestParams.getFileParams().size() > 0) {
                    Map<String, RequestParams.FileWrapper> fileParams = requestParams.getFileParams();
                    for (ConcurrentHashMap.Entry<String, RequestParams.FileWrapper> entry : fileParams.entrySet()) {
                        postFormBuilder.addFile(entry.getKey(), entry.getValue().customFileName, entry.getValue().file);
                    }
                }

                postFormBuilder.url(url);

                Map<String, String> _params = requestParams.getUrlParams();
                if (_params.size() > 0) {
                    postFormBuilder.addParams(_params);
                }
                requestCall = postFormBuilder.build();
                requestCall.execute(callback);
                break;
            case METHOD_PUT:
                OtherRequestBuilder putRequestBuilder = OkHttpUtils.put().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        putRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = putRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = putRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(requestParams.getUrlParams()))).addParams(requestParams.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_PATCH:
                OtherRequestBuilder pathRequestBuilder = OkHttpUtils.patch().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        pathRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = pathRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = pathRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(requestParams.getUrlParams()))).addParams(requestParams.getUrlParams()).build();
                requestCall.execute(callback);
                break;
            case METHOD_DELETE:
                OtherRequestBuilder deleteRequestBuilder = OkHttpUtils.delete().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        deleteRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = deleteRequestBuilder.addParams(params.getUrlParams()).build();
                requestCall = deleteRequestBuilder.requestBody(RequestBody.create(null, createRequestBody(requestParams.getUrlParams()))).addParams(requestParams.getUrlParams()).build();
                requestCall.execute(callback);
                break;
        }

        return requestCall;
    }


    public static RequestCall createRawDataRequest(int method, String url, HeaderParams headerParams, MediaType mediaType, String text, Callback callback) {
        RequestCall requestCall = null;
        switch (method) {
            case METHOD_POST:
                OtherRequestBuilder postRequestBuilder = OkHttpUtils.postRaw().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        postRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = putRequestBuilder.addParams(params.getUrlParams()).build();
                RequestBody postRequestBody = RequestBody.create(mediaType, text);
                requestCall = postRequestBuilder.requestBody(postRequestBody).build();
                requestCall.execute(callback);
                break;
            case METHOD_PUT:
                OtherRequestBuilder putRequestBuilder = OkHttpUtils.put().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        putRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = putRequestBuilder.addParams(params.getUrlParams()).build();
                RequestBody putRequestBody = RequestBody.create(mediaType, text);
                requestCall = putRequestBuilder.requestBody(putRequestBody).build();
                requestCall.execute(callback);
                break;
            case METHOD_PATCH:
                OtherRequestBuilder pathRequestBuilder = OkHttpUtils.patch().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        pathRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = pathRequestBuilder.addParams(params.getUrlParams()).build();
                RequestBody pathRequestBody = RequestBody.create(mediaType, text);
                requestCall = pathRequestBuilder.requestBody(pathRequestBody).build();
                requestCall.execute(callback);
                break;
            case METHOD_DELETE:
                OtherRequestBuilder deleteRequestBuilder = OkHttpUtils.delete().url(url);
                if (headerParams != null) {
                    for (ConcurrentHashMap.Entry<String, String> entry : headerParams.getHeaderParams().entrySet()) {
                        deleteRequestBuilder.addHeader(entry.getKey(), entry.getValue());
                    }
                }
//                requestCall = deleteRequestBuilder.addParams(params.getUrlParams()).build();
                RequestBody deleteRequestBody = RequestBody.create(mediaType, text);
                requestCall = deleteRequestBuilder.requestBody(deleteRequestBody).build();
                requestCall.execute(callback);
                break;
        }

        return requestCall;
    }

    /**
     * Stop request
     *
     * @param tag
     */
    public static void stopRequest(Object tag) {
        if (client == null) return;
        OkHttpUtils.getInstance(client).cancelTag(tag);
    }

    /**
     * Stop all request
     */
    public static void stopAllRequest() {
        if (client == null) return;

        OkHttpUtils.getInstance(client).cancelAll();
    }


//    public static boolean isNetworkAvailable(Context context) {
//        boolean has = false;
//        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo info = connectivity.getActiveNetworkInfo();
//        if (info == null) return false;
//
//        int netType = info.getType();
//        int netSubtype = info.getSubtype();
//        if (netType == ConnectivityManager.TYPE_WIFI) {
//            has = info.isConnected();
//        }
//        return has;
//    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null && info.isConnected()) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    protected static String createRequestBody(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        if (map != null) {
            for (ConcurrentHashMap.Entry<String, String> entry : map.entrySet()) {
                try {
                    sb.append(entry.getKey())
                            .append("=")
                            .append(URLEncoder.encode(entry.getValue(), "utf-8"));
                    sb.append("&");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

}
