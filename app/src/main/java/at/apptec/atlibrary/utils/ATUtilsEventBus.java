/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package at.apptec.atlibrary.utils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by chen on 17.01.18.
 */

public class ATUtilsEventBus {

    public ATUtilsEventBus() {
    }

    /**
     * unregister
     */
    public static void unregister(Object subscriber) {
        if (EventBus.getDefault().isRegistered(subscriber)) EventBus.getDefault().unregister(subscriber);
    }

    /**
     * register
     */
    public static void register(Object subscriber) {
        if (!EventBus.getDefault().isRegistered(subscriber)) EventBus.getDefault().register(subscriber);
    }

    /**
     * send event
     */
    public static void post(Object event) {
        EventBus.getDefault().post(event);
    }

    /**
     * send sticky event
     */
    public static void postSticky(Object event) {
        EventBus.getDefault().postSticky(event);
    }

    /**
     * is sticky event exist
     */
    public static boolean isStickyExist(Class<?> cls) {
        return EventBus.getDefault().getStickyEvent(cls) != null;
    }

    /**
     * remove sticky event
     */
    public static void removeSticky(Class<?> cls) {
        if (EventBus.getDefault().getStickyEvent(cls) != null) {
            EventBus.getDefault().removeStickyEvent(cls);
        }
    }
}
