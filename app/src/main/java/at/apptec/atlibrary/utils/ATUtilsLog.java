/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2018 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package at.apptec.atlibrary.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by chen on 05/03/15.
 */
public class ATUtilsLog {
    private static final int JSON_INDENT = 2;
    private static final int CHUNK_SIZE = 156;
    private static final char TOP_LEFT_CORNER = '┌';
    private static final char BOTTOM_LEFT_CORNER = '└';
    private static final char MIDDLE_CORNER = '├';
    private static final char HORIZONTAL_LINE = '│';
    private static final String DOUBLE_DIVIDER = "───────────────────────────────────────────────────────────────────────────────";
    private static final String SINGLE_DIVIDER = "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄";
    private static final String TOP_BORDER = TOP_LEFT_CORNER + DOUBLE_DIVIDER + DOUBLE_DIVIDER;
    private static final String BOTTOM_BORDER = BOTTOM_LEFT_CORNER + DOUBLE_DIVIDER + DOUBLE_DIVIDER;
    private static final String MIDDLE_BORDER = MIDDLE_CORNER + SINGLE_DIVIDER + SINGLE_DIVIDER;


    /**
     * Debug switch
     */
    private static boolean DEBUG = true;

    /**
     * Info switch
     */
    private static boolean INFO = true;

    /**
     * Error switch
     */
    private static boolean ERROR = true;


    /**
     * Start time
     */
    private static long startLogTimeInMillis = 0;


    private static boolean isPrepareLog = false;


    /**
     * Debug log
     *
     * @param tag
     * @param message
     */
    public static void debug(String tag, String message) {
        if (DEBUG) {
            Log.d(tag, message);
        }
    }

    /**
     * Debug log
     *
     * @param context
     * @param message
     */
    public static void debug(Context context, String message) {
        String tag = context.getClass().getSimpleName();
        debug(tag, message);
    }

    /**
     * Debug log
     *
     * @param clazz
     * @param message
     */
    public static void debug(Class<?> clazz, String message) {
        String tag = clazz.getSimpleName();
        debug(tag, message);
    }

    /**
     * Debug log
     *
     * @param message
     */
    public static void debug(String message) {
        String tag = generateTag(getCallerStackTraceElement());
        debug(tag, message, true);
    }

    public static void debug(String message, boolean wellFormat) {
        String tag = generateTag(getCallerStackTraceElement());
        debug(tag, message, wellFormat);
    }

    public static void debug(String tag, String message, boolean wellFormat) {
        if (DEBUG) {
            if (wellFormat) {
                Log.d("DEBUG", "-");
                Log.d("DEBUG", TOP_BORDER);
                Log.d("DEBUG", HORIZONTAL_LINE + " " + tag);
                Log.d("DEBUG", MIDDLE_BORDER);

                byte[] bytes = message.getBytes();
                int length = bytes.length;
                if (length <= CHUNK_SIZE) {
                    String[] lines = message.split(System.getProperty("line.separator"));
                    for (String line : lines) {
                        Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                    }
                } else {
                    for (int i = 0; i < length; i += CHUNK_SIZE) {
                        int count = Math.min(length - i, CHUNK_SIZE);

                        String[] lines = new String(bytes, i, count).split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                        }
                    }
                }
                Log.d("DEBUG", BOTTOM_BORDER);
            } else {
                Log.d(tag, message);
            }
        }
    }


    /**
     * info log
     *
     * @param tag
     * @param message
     */
    public static void info(String tag, String message) {
        if (INFO) {
            Log.i(tag, message);
        }
    }

    /**
     * info log
     *
     * @param context
     * @param message
     */
    public static void info(Context context, String message) {
        String tag = context.getClass().getSimpleName();
        info(tag, message);
    }

    /**
     * info log
     *
     * @param clazz
     * @param message
     */
    public static void info(Class<?> clazz, String message) {
        String tag = clazz.getSimpleName();
        info(tag, message);
    }

    /**
     * info log
     *
     * @param message
     */
    public static void info(String message) {
        String tag = generateTag(getCallerStackTraceElement());
        info(tag, message, true);
    }

    public static void info(String message, boolean wellFormat) {
        String tag = generateTag(getCallerStackTraceElement());
        info(tag, message, wellFormat);
    }

    public static void info(String tag, String message, boolean wellFormat) {
        if (INFO) {

            if (wellFormat) {
                Log.i("INFO", "-");
                Log.i("INFO", TOP_BORDER);
                Log.i("INFO", HORIZONTAL_LINE + " " + tag);
                Log.i("INFO", MIDDLE_BORDER);

                byte[] bytes = message.getBytes();
                int length = bytes.length;
                if (length <= CHUNK_SIZE) {
                    String[] lines = message.split(System.getProperty("line.separator"));
                    for (String line : lines) {
                        Log.i("INFO", HORIZONTAL_LINE + " " + line);
                    }
                } else {
                    for (int i = 0; i < length; i += CHUNK_SIZE) {
                        int count = Math.min(length - i, CHUNK_SIZE);

                        String[] lines = new String(bytes, i, count).split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            Log.i("INFO", HORIZONTAL_LINE + " " + line);
                        }
                    }
                }
                Log.i("INFO", BOTTOM_BORDER);
            } else {
                Log.i(tag, message);
            }

        }
    }


    /**
     * error log
     *
     * @param tag
     * @param message
     */
    public static void error(String tag, String message) {
        if (ERROR) {
            Log.e(tag, message);
        }
    }

    /**
     * error log
     *
     * @param context
     * @param message
     */
    public static void error(Context context, String message) {
        String tag = context.getClass().getSimpleName();
        error(tag, message);
    }

    /**
     * error log
     *
     * @param clazz
     * @param message
     */
    public static void error(Class<?> clazz, String message) {
        String tag = clazz.getSimpleName();
        error(tag, message);
    }


    /**
     * error log
     *
     * @param message
     */
    public static void error(String message) {
        String tag = generateTag(getCallerStackTraceElement());
        error(tag, message, false);
    }

    public static void error(String message, boolean wellFormat) {
        String tag = generateTag(getCallerStackTraceElement());
        error(tag, message, wellFormat);
    }

    public static void error(String tag, String message, boolean wellFormat) {
        if (ERROR) {
            if (wellFormat) {
                Log.e("ERROR", "-");
                Log.e("ERROR", TOP_BORDER);
                Log.e("ERROR", HORIZONTAL_LINE + " " + tag);
                Log.e("ERROR", MIDDLE_BORDER);

                byte[] bytes = message.getBytes();
                int length = bytes.length;
                if (length <= CHUNK_SIZE) {
                    String[] lines = message.split(System.getProperty("line.separator"));
                    for (String line : lines) {
                        Log.e("ERROR", HORIZONTAL_LINE + " " + line);
                    }
                } else {
                    for (int i = 0; i < length; i += CHUNK_SIZE) {
                        int count = Math.min(length - i, CHUNK_SIZE);

                        String[] lines = new String(bytes, i, count).split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            Log.e("ERROR", HORIZONTAL_LINE + " " + line);
                        }
                    }
                }
                Log.e("ERROR", BOTTOM_BORDER);
            } else {
                Log.e(tag, message);
            }
        }
    }


    /**
     * Record the current time in milliseconds.
     */
    public static void prepareLog(String tag) {
        isPrepareLog = true;
        Calendar current = Calendar.getInstance();
        startLogTimeInMillis = current.getTimeInMillis();
        if (DEBUG) Log.d(tag, "Log start：" + startLogTimeInMillis);
    }

    /**
     * Record the current time in milliseconds.
     */
    public static void prepareLog(Context context) {
        String tag = context.getClass().getSimpleName();
        prepareLog(tag);
    }

    /**
     * Record the current time in milliseconds.
     */
    public static void prepareLog(Class<?> clazz) {
        String tag = clazz.getSimpleName();
        prepareLog(tag);
    }

//    /**
//     * Prints the execution time in milliseconds. ** YOU NEED TO CALL "prepareLog()" FIRST**.
//     *
//     * @param tag       tag
//     * @param message   message
//     * @param printTime print time or not
//     */
//    public static void debug(String tag, String message, boolean printTime) {
//        if (printTime) {
//            if (startLogTimeInMillis == 0 || !isPrepareLog) {
//                if (ERROR) Log.e(tag, "** YOU NEED TO CALL \"prepareLog()\" FIRST**");
//            }
//
//            Calendar current = Calendar.getInstance();
//            long endLogTimeInMillis = current.getTimeInMillis();
//            if (DEBUG)
//                Log.d(tag, message + ":" + (endLogTimeInMillis - startLogTimeInMillis) + "ms");
//
//        } else {
//            debug(tag, message);
//        }
//
//        isPrepareLog = false;
//    }
//
//
//    /**
//     * Prints the execution time in milliseconds. ** YOU NEED TO CALL "prepareLog()" FIRST**.
//     *
//     * @param context   context
//     * @param message   message
//     * @param printTime print time or not
//     */
//    public static void debug(Context context, String message, boolean printTime) {
//        String tag = context.getClass().getSimpleName();
//        debug(tag, message, printTime);
//    }
//
//    /**
//     * Prints the execution time in milliseconds. ** YOU NEED TO CALL "prepareLog()" FIRST**.
//     *
//     * @param clazz     class
//     * @param message   message
//     * @param printTime print time or not
//     */
//    public static void debug(Class<?> clazz, String message, boolean printTime) {
//        String tag = clazz.getSimpleName();
//        debug(tag, message, printTime);
//    }

    /**
     * debug log switch
     *
     * @param debug switch
     */
    public static void debug(boolean debug) {
        DEBUG = debug;
    }

    /**
     * info log switch
     *
     * @param info switch
     */
    public static void info(boolean info) {
        INFO = info;
    }

    /**
     * error log switch
     *
     * @param error switch
     */
    public static void error(boolean error) {
        ERROR = error;
    }

    /**
     * Set log switch
     *
     * @param debug debug log switch
     * @param info  info log switch
     * @param error error log switch
     */
    public static void setVerbose(boolean debug, boolean info, boolean error) {
        DEBUG = debug;
        INFO = info;
        ERROR = error;
    }

    /**
     * Open all log
     */
    public static void openAll() {
        DEBUG = true;
        INFO = true;
        ERROR = true;
    }

    /**
     * Close all log
     */
    public static void closeAll() {
        DEBUG = false;
        INFO = false;
        ERROR = false;
    }


    private static String customTagPrefix = "";

    private static String generateTag(StackTraceElement caller) {
        String tag = "%s.%s(Line:%d)";
        String callerClazzName = caller.getClassName();
        callerClazzName = callerClazzName.substring(callerClazzName
                .lastIndexOf(".") + 1);
        tag = String.format(tag, callerClazzName, caller.getMethodName(),
                caller.getLineNumber());
        tag = TextUtils.isEmpty(customTagPrefix) ? tag : customTagPrefix + ":"
                + tag;
        return tag;
    }

    public static StackTraceElement getCurrentStackTraceElement() {
        return Thread.currentThread().getStackTrace()[3];
    }

    public static StackTraceElement getCallerStackTraceElement() {
        return Thread.currentThread().getStackTrace()[4];
    }

    public static void json(String header, String json) {
        if (TextUtils.isEmpty(json)) {
            error(generateTag(getCallerStackTraceElement()), "Empty/Null json content");
            return;
        }
        try {
            json = json.trim();
            if (json.startsWith("{")) {
                JSONObject jsonObject = new JSONObject(json);
                String message = jsonObject.toString(JSON_INDENT);
                if (DEBUG) {
                    String tag = generateTag(getCallerStackTraceElement());
                    Log.d("DEBUG", TOP_BORDER);
                    Log.d("DEBUG", HORIZONTAL_LINE + " " + tag + " - " + header);
                    Log.d("DEBUG", MIDDLE_BORDER);

                    byte[] bytes = message.getBytes();
                    int length = bytes.length;
                    if (length <= CHUNK_SIZE) {
                        String[] lines = message.split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                        }
                    } else {
                        for (int i = 0; i < length; i += CHUNK_SIZE) {
                            int count = Math.min(length - i, CHUNK_SIZE);

                            String[] lines = new String(bytes, i, count).split(System.getProperty("line.separator"));
                            for (String line : lines) {
                                Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                            }
                        }
                    }
                    Log.d("DEBUG", BOTTOM_BORDER);
                }
                return;
            }
            if (json.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(json);
                String message = jsonArray.toString(JSON_INDENT);
                if (DEBUG) {
                    String tag = generateTag(getCallerStackTraceElement());
                    Log.d("DEBUG", TOP_BORDER);
                    Log.d("DEBUG", HORIZONTAL_LINE + " " + tag + " - " + header);
                    Log.d("DEBUG", MIDDLE_BORDER);

                    byte[] bytes = message.getBytes();
                    int length = bytes.length;
                    if (length <= CHUNK_SIZE) {
                        String[] lines = message.split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                        }
                    } else {
                        for (int i = 0; i < length; i += CHUNK_SIZE) {
                            int count = Math.min(length - i, CHUNK_SIZE);

                            String[] lines = new String(bytes, i, count).split(System.getProperty("line.separator"));
                            for (String line : lines) {
                                Log.d("DEBUG", HORIZONTAL_LINE + " " + line);
                            }
                        }
                    }
                    Log.d("DEBUG", BOTTOM_BORDER);
                }
                return;
            }
            error(generateTag(getCallerStackTraceElement()), "Invalid Json");
        } catch (JSONException e) {
            error(generateTag(getCallerStackTraceElement()), "Invalid Json");
        }
    }
}
