/*************************************************************************
 * apptec.at android application                                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Copyright (c) 2020 apptec GmbH: www.apptec.at                         *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * This source file is part of the apptec.at android app. You are not    *
 * allowed to copy or use parts of the sourcecode or the sourcecode as   *
 * a whole. This source code is intellectual property of apptec GmbH. If *
 * you want to use parts of this source code or license the whole app    *
 * contact apptec GmbH at: office@apptec.at for licensing.               *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *
 * Author: chen <chen.xue@apptec.at>                                     *
 *************************************************************************/

package at.apptec.atlibrary.okhttp.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chen on 09.03.18.
 */

public class HeaderParams implements Serializable {
    protected final ConcurrentHashMap<String, String> headerParams = new ConcurrentHashMap<String, String>();

    public HeaderParams() {
        this((Map<String, String>) null);
    }

    public HeaderParams(Map<String, String> source) {
        if (source != null) {
            for (Map.Entry<String, String> entry : source.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public HeaderParams(final String key, final String value) {
        this(new HashMap<String, String>() {{
            put(key, value);
        }});
    }

    public void put(String key, String value) {
        if (key != null && value != null) {
            headerParams.put(key, value);
        }
    }

    public void put(String key, int value) {
        if (key != null) {
            headerParams.put(key, String.valueOf(value));
        }
    }

    public void put(String key, long value) {
        if (key != null) {
            headerParams.put(key, String.valueOf(value));
        }
    }

    public void remove(String key) {
        headerParams.remove(key);
        headerParams.remove(key);
    }

    public boolean has(String key) {
        return headerParams.get(key) != null ||
                headerParams.get(key) != null;
    }


    public Map<String, String> getHeaderParams() {
        return headerParams;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (ConcurrentHashMap.Entry<String, String> entry : headerParams.entrySet()) {
            if (result.length() == 0)
                result.append(entry.getKey());

            result.append("; ");
            result.append(entry.getValue());
        }

        return result.toString();
    }

}
